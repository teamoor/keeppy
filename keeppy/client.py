import socket
import sys
from select import select
from threading import Thread, Event
import time


def reader(sock, timeout=3.0):
    fd = sock.fileno()
    checked_at = time.time()
    while True:
        try:
            r, _, x = select([fd], [], [fd], 0)
        except Exception as e:
            if not sock._closed:
                print(f'reader: {fd} select error: {e}', file=sys.stderr)
            return
        if timeout is not None and time.time() - checked_at >= timeout:
            return
        if x:
            print(f'x', file=sys.stderr, end='')
            continue
        if not r:
            continue

        try:
            data = sock.recv(4096).decode()
        except Exception as e:
            print(f'{fd} recv error: {e}', file=sys.stderr)
            return
        if not data:
            print(f'{fd} connection lost', file=sys.stderr)
            return 

        checked_at = time.time()
        # print(f'recvd {len(data)}', file=sys.stderr)
        yield data


def writer(sock, get_message):
    fd = sock.fileno()
    msg = None 
    sent = 0
    while True:
        try:
            _, w, x = select([], [fd], [fd], 0.0)
        except Exception as e:
            print(f'writer: {fd} select error: {e}', file=sys.stderr)
            return
        if x:
            print(f'x', file=sys.stderr, end='')
            continue
        if not w:
            continue
        try:
            msg = get_message()
        except (EOFError, StopIteration):
            return 

        line = f'{msg}\n'.encode()
        try:
            sock.sendall(line)
        except BrokenPipeError:
            print(f'writer: {fd} broken pipe', file=sys.stderr)
            return
        sent += len(line)


def client(path, get_message, timeout=3.0):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

    print(f'connecting…', file=sys.stderr, end='')
    sock.connect(path)
    print(f' OK', file=sys.stderr)

    Thread(target=writer, args=(sock, get_message), daemon=True).start()
    return reader(sock, timeout)


def main(path):
    try:
        for chunk in client(path, input, timeout=None):
            print(chunk, end='')
            sys.stdout.flush()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main(*sys.argv[1:])
