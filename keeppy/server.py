import fcntl
import os
import pathlib
import sys
import socket
import time

from getpass import getpass
from subprocess import Popen, PIPE
from threading import Thread
from queue import Queue, Empty
from select import select 


def main(dbpath, socket_path, timeout=300):
    timeout = int(timeout)
    checked_at = None 

    child = Popen(
        f'keepassxc-cli open {dbpath}'.split(),
        stdin=PIPE,
        stdout=PIPE,
        stderr=PIPE,
    )

    inq = Queue()
    outq = Queue()
    errq = Queue()

    def reader():
        fd = child.stdout.fileno()
        while True:
            # XXX For some reason right after startup the fd is not read list 
            # while it's actually possible to do a read (os x)
            # r, _, x = select([fd], [], [fd], 1)
            # if x:
                # print(f'{fd} exc state', file=sys.stderr)
            # if not r:
                # print(f'{fd} not read-ready', file=sys.stderr)
                # continue
            data = child.stdout.read(1)
            if not data:
                print(f'{fd} no data', file=sys.stderr)
                break

            # print(f'read {len(data)}', file=sys.stderr)
            outq.put(data.decode())

    def writer():
        fd = child.stdin.fileno()
        while True:
            try:
                data = inq.get(block=True, timeout=0.1)
            except Empty:
                continue
            child.stdin.write(data.encode())
            child.stdin.flush()

    def ereader():
        fd = child.stderr.fileno()
        flag = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
        while True:
            r, _, x = select([fd], [], [fd], 0.1)
            if not r:
                continue
            data = child.stderr.read(4096).decode()
            if not data:
                print(f'{fd} no data', file=sys.stderr)
                break

            print(f'{data}', file=sys.stderr, end='')
            sys.stderr.flush()
            errq.put(data)

    def prompt():
        # Unlock DB with password
        # Original prompt will be on the tty since we reprint it
        inq.put(f'{getpass("")}\n')

    def found_errors():
        errors = ''
        while True:
            try:
                errors += errq.get(timeout=1)
            except Empty:
                break
        if "Error while reading the database:" in errors:
            return True
        return False

    def bind():
        path = pathlib.Path(socket_path)
        # path.unlink(missing_ok=True)

        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(f'{path}')
        sock.listen()
        return sock

    def handle_client(conn):
        fd = conn.fileno()
        print(f'{fd} connected', file=sys.stderr)
        checked_at = time.time()
        while True:
            try:
                r, w, x = select([fd], [fd], [fd], 0.0)
            except Exception as e:
                print(f'{fd} select error: {e}', file=sys.stderr)
                break

            if time.time() - checked_at >= timeout:
                print(f'{fd} timed out', file=sys.stderr)
                conn.close()
                break

            if r: 
                data = conn.recv(64)
                if not data: 
                    print(f'{fd} lost', file=sys.stderr)
                    break
                inq.put(data.decode())

            # Should be almost always true for a healthy socket
            # Means there's still some space in the network output buffer 
            if w:
                try:
                    out = outq.get(timeout=0.01)
                except Empty:
                    continue
                try:
                    conn.sendall(out.encode())
                except BrokenPipeError:
                    print(f'{fd} broken pipe', file=sys.stderr)
                    break
                # print(f'sent {len(out.encode())}', file=sys.stderr)
                checked_at = time.time()

            if x:
                print(f'x', file=sys.stderr, end='')

        # Clear output queue
        while True:
            try:
                outq.get(block=False)
            except Empty:
                break

    Thread(target=ereader, daemon=True).start()
    Thread(target=reader, daemon=True).start()
    Thread(target=writer, daemon=True).start()

    prompt()

    if found_errors():
        return

    sock = bind()

    try:
        while True:
            conn, addr = sock.accept()
            with conn:
                handle_client(conn)
    except KeyboardInterrupt:
        pass


def entrypoint():
    main(*sys.argv[1:])


if __name__ == '__main__':
    entrypoint()
