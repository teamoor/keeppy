from setuptools import setup, find_packages

setup(
	name='keeppy',
	setup_requires=['setuptools_scm'],
	use_scm_version=True,
	packages=find_packages(),
	entry_points=dict(
		console_scripts=[
			'keeppy=keppy.server:main',
			'keeppyc=keppy.cleint:main',
		]
	),
)
